$('.station').css('visibility','hidden');
$(window).scroll(function(){
 var windowHeight = $(window).height();
 var topWindow = $(window).scrollTop();
 $('.station').each(function(){
  var target = $(this).offset().top;
  if(topWindow > target-windowHeight+100){
   $(this).addClass("fadeInDown");
  }
 });
});
$(function () {
  $('.dropdown').change(function() {
    var url = $('.dropdown').val();
    $(location).attr("href", url);
  });
});
