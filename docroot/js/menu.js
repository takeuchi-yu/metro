(function($) {
    $(function(){
        $(".open").click(function(){
            $(".pull_menu_nav").slideDown();
        });
        $(".close").click(function(){
            $(".pull_menu_nav").slideUp();
        });
    });
})(jQuery);