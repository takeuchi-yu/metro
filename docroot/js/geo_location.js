function getGeoFirst() {
    query = location.search.substring(1);
    if (query.indexOf("lt=") != -1 && query.indexOf("ln=") != -1) {
    } else if (query.indexOf("geo=off") == -1) {
        navigator.geolocation.getCurrentPosition(successCallback, errorCallback);
    }
}
function successCallback(position) { 
    var latitude = position.coords.latitude;
    var longitude = position.coords.longitude;
    if (latitude && longitude) {
        var url = "?lt=" + latitude + "&ln=" + longitude;
        var parameters = query.split( '&' );
        for (var i = 0; i < parameters.length; i++) {
            if (parameters[ i ].indexOf("lt=") == -1 && parameters[ i ].indexOf("ln=") == -1) {
                url += "&" + parameters[ i ];
            }
        }
        location.href = url;
    }
}
function errorCallback(error) {
    if (query.length > 0) {
        location.href = "?geo=off" + "&" + query;
    } else {
        location.href = "?geo=off";
    }
}
// onclick
function getGeo(param) {
    query = param;
    navigator.geolocation.getCurrentPosition(successCallback, errorCallback);
}
