function displayBookmark($line){
    var $cookieName = 'chika-toilet';
    var $id = "bookmark_0";
    if(checkLineCookie($cookieName, $line)){
        document.getElementById($id).className = 'fa fa-star fa-5x bookmark';
    }else{
        document.getElementById($id).className = 'fa fa-star-o fa-5x bookmark';
    }
}

function checkLineCookie($cookieName, $getCookieValue){
    var $cookieValue = getCookie($cookieName);
    if($cookieValue){
        if($cookieValue == $getCookieValue){
            return true;
        }
    }
    return false;
}

function setLineCookie($setCookieValue){
    var $cookieName = 'chika-toilet';
    var $days = 365;
    var $getCookieValue = getCookie($cookieName);
    if($getCookieValue){
        if($getCookieValue == $setCookieValue){
            $setCookieValue = '';
        }
    }
    setCookie($cookieName, $setCookieValue, $days);
    displayBookmark($setCookieValue);
}

function setCookie($cookieName, $cookieValue, $days){
    var $dateObject = new Date();
    $dateObject.setTime( $dateObject.getTime() + ( $days*24*60*60*1000 ) );
    var $expires = "expires=" + $dateObject.toGMTString();
    document.cookie = $cookieName + "=" + $cookieValue + "; " + $expires;
}

function getCookie($cookieName){
    var $cookies = document.cookie.split(';');
    for(var $i=0; $i < $cookies.length; $i++){
        var $cookie = $cookies[$i].trim().split( '=' );
        if($cookie[0] == $cookieName){
            return $cookie[1];
        }
    }
    return "";
}

function deleteCookie($cookieName){
    document.cookie = $cookieName + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT"; 
}

function displayCookie($cookieName, $output){
    var $cookieValue = getCookie($cookieName);
    document.getElementById($output).innerHTML = $cookieValue;
}
