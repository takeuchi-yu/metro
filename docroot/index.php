<?php
/**
 * index.php
 */
require_once '../configure/config/config.php';

try {
    if (isset($_GET['type']) && $_GET['type'] == TYPE_LINE) {
        require_once CONTROLLER_DIR . 'Line_Controller.php';
        $controller = new Line_Controller();
    } else if (isset($_GET['type']) && $_GET['type'] == TYPE_SITUATION) {
        require_once CONTROLLER_DIR . 'Situation_Controller.php';
        $controller = new Situation_Controller();
    } else if (isset($_GET['type']) && $_GET['type'] == TYPE_HELP) {
        require_once CONTROLLER_DIR . 'Help_Controller.php';
        $controller = new Help_Controller();
    } else {
        require_once CONTROLLER_DIR . 'Index_Controller.php';
        $controller = new Index_Controller();
    }

    $controller->main();
} catch (Exception $e) {
    $message = '[' . date('Y-m-d H:i:s') . "]\t";
    $message .= $e->getMessage();
    error_log($message, 3, LOG_DIR . 'error_log');

    require_once CONTROLLER_DIR . 'Error_Controller.php';
    $controller = new Error_Controller();
    $controller->main();
}
