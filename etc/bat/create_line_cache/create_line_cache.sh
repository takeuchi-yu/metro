#!/bin/sh

MAX_TIME=180 # タイムアウト

function log()
{
    echo "`date +"%Y/%m/%d %H:%M:%S"` [$1] $2"
}

log "start" $0

for line in "Fukutoshin" "Hibiya" "Ginza" "Marunouchi" "Namboku" "Tozai" "Yurakucho" "Hanzomon" "Chiyoda"
do
  url="http://54.64.204.235/test/Situation/?type=line&rail_way=$line"
  log $line $url
  curl -s $url --max-time $MAX_TIME -o /dev/null -w "`date +"%Y/%m/%d %H:%M:%S"` [$line] %{time_total}sec\n"

  if [ $? -eq 0 ]; then
    log $line "success"
  else
    log $line "failure"
  fi
done

log "end" $0

