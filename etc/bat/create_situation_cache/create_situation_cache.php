<?php
/**
 * シチューション検索用キャッシュファイル作成バッチ.
 *
 * @author gnavi11
 * @package gnavi11-metro
 * @version $Id$
 */

define ('PRODUCT_CONFIGURE_DIR', $argv[2]);

require_once PRODUCT_CONFIGURE_DIR.'config/config.php';
require_once PRODUCT_CONFIGURE_DIR.'controller/Situation_Controller.php';

class Create_Situation_Cache extends Situation_Controller {

    /**
     * メイン処理
     *
     * @return Situation_Controller
     */
    public function main($file_path){
        $radius = 200;
        $points = $this->getPointsFromCSV($file_path);

        $this->printLog('[count line] '.$file_path.' '.count($points));

        foreach($points as $id => $point){
            $this->printLog('['.$id.'] '.$point[0].' '.$point[1].'('.$point[2].', '.$point[3].')');

            sleep(3); // 近い駅取得APIたたく前も必要でした..(overrideするのめんどいのでここでやっちゃう)

            // 近い駅取得
            $nearStations = $this->getNearStations($point[2], $point[3], $radius);

            // 施設情報取得(キャッシュ作成)
            $stationFacilities = $this->getStationFacility($nearStations);
        }
        return true;
    }

    /* ログ用 */
    private function printLog($string){
        print $string."\n";
    }

    /* 指定のCSVファイルを読み込む */
    private function getPointsFromCSV($filepath){
        $data = array();
        if (($handle = fopen($filepath, "r")) !== false) {
            while (($line = fgetcsv($handle, 1000, ",")) !== false) {
                $data[] = $line;
            }
            fclose($handle);
        }
        return $data;
    }

    /**
     * 駅の施設情報を取得(Situation_Controllerからこぴってきた)
     * @param array $list 駅のリスト
     * @return array 駅の施設情報(categoryNameがトイレの情報のみ)
     */
    protected function getStationFacility($list){
        $facility_list = array();

        $cache = new Cache();

        foreach ($list as $id => $data) {
            $tmp = '';
            $tmp = $cache->getData($id);

            // キャッシュない場合は、APIからデータ取得してごにょごにょする
            if(!$tmp) {
                $this->printLog('    0 '.$id);
                $api = new MetroAPI();
                $res = array();

                sleep(3); // 503対応

                $res = $api->setType(METRO_API_TYPE_DATAPOINTS)->getData(array(), $data['odpt:facility']);

                // 駅施設情報から、"対象の沿線"かつ"ug:Toilet" の情報だけに絞り込む
                $info = array();
                foreach($res[0]->{'odpt:barrierfreeFacility'} as $i => $detail) {
                    if($detail->{'@type'} == 'ug:Toilet' && strstr($detail->{'owl:sameAs'}, 'odpt.StationFacility:'.$id)) {
                        $info[$detail->{'owl:sameAs'}] = $detail;
                    }
                }

                // トイレがない場合だってあるのさ
                if(!empty($info)) {
                    $formated = $this->facilityArrayToStructuredData($id, $data, $info);

                    $cache->setData($id, $formated); // ここまで構造化してからキャッシュ!
                    $facility_list[$id] = $formated;
                }
            } else {
                $this->printLog('    1 '.$id);
                $facility_list[$id] = $tmp;
            }
        }
        return $facility_list;
    }
}

$file_path = $argv[1];

$maker = new Create_Situation_Cache();
$maker->main($file_path);

