#!/bin/sh

# 実行例
# ./create_situation_cache.sh > log/`date +"%Y%m%d%H%M"`_create_situation_cache.log 2>&1

CONFIGURE_DIR="/home/www/test/Situation/configure/"
CACHE_DIR="/home/cache/test/Situation/"
BATCH_DIR="/home/www/bat/bin/create_situation_cache/"

echo "`date +"%Y/%m/%d %H:%M:%S"` [start]"
BEFORE=`find ${CACHE_DIR} -type f | wc -l`
echo "[cache file] ${BEFORE}"

cd ${CONFIGURE_DIR}
sudo -u apache /usr/bin/php ${BATCH_DIR}create_situation_cache.php ${BATCH_DIR}all_stations.csv ${CONFIGURE_DIR}
#/usr/bin/php ${BATCH_DIR}create_situation_cache.php ${BATCH_DIR}all_stations_20141105.csv ${CONFIGURE_DIR}

AFTER=`find ${CACHE_DIR} -type f | wc -l`
echo "[cache file] ${AFTER}"
echo "`date +"%Y/%m/%d %H:%M:%S"` [end]"

