require 'open-uri'
require 'json'

html = open('https://api.tokyometroapp.jp/api/v2/datapoints?rdf:type=odpt:Station&acl:consumerKey=xxx').read
json = JSON.parser.new(html)
#puts json.class

hash =  json.parse()
#puts hash

hash.each do | station |
  #puts station['odpt:railway'].match(/.*\..*\.(.*)/)[1]
  puts station['odpt:railway'].match(/.*\..*\.(.*)/)[1] + ',' + station['dc:title'] + ',' + station['geo:lat'].to_s + ',' + station['geo:long'].to_s
end
