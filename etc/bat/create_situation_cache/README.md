# シチュエーション検索用キャッシュファイル作成バッチ

## データ準備
- メトロAPIで取得したJSONから必要な情報だけ抜き出してcsvファイル作成

    ./get_metro_data_as_csv/getMetroApiData.rb > all_stations.csv

## バッチ実行
- `Situation_Controller.php` をoverrideして、メトロAPIコール時にsleepを追加している
- 実行例

    sh /home/www/bat/bin/create_situation_cache/create_situation_cache.sh > /home/www/bat/bin/create_situation_cache/log/`date +"%Y%m%d%H%M"`_create_situation_cache.log 2>&1


