<?php
require_once 'define.php';

define('TYPE_LINE',      'line');
define('TYPE_SITUATION', 'situation');
define('TYPE_HELP',      'help');

define('CONFIGURE_DIR', BASE_DIR . 'configure/');
define('DOCROOT_DIR', BASE_DIR . 'docroot/');
define('CONFIG_DIR', CONFIGURE_DIR . 'config/');
define('CONTROLLER_DIR', CONFIGURE_DIR . 'controller/');
define('LIB_DIR', CONFIGURE_DIR . 'lib/');
define('MODEL_DIR', CONFIGURE_DIR . 'model/');
define('VIEW_DIR', CONFIGURE_DIR . 'view/');

define('SMARTY_LIB_DIR', LIB_DIR . 'Smarty-3.1.19/libs/');
define('SMARTY_CACHE_DIR', SMARTY_BASE_DIR . 'cache/');
define('SMARTY_COMPILE_DIR', SMARTY_BASE_DIR . 'compile/');
require_once SMARTY_LIB_DIR . 'Smarty.class.php';

define('PEAR_LIB_DIR', LIB_DIR . 'Pear-1.9.5/');
define('CACHE_DIR', CACHE_BASE_DIR);
define('CACHE_LIFE_TIME', 86400);
define('CACHE_DIR_LEVEL', 1);
define('CACHE_FILE_NAME_PROTECTION', TRUE);
define('CACHE_AUTO_SERIALIZATION', TRUE);
define('CACHE_AUTO_CLEANING_FACTOR', 0);
define('CACHE_DIR_UMASK', 0777);
set_include_path (PEAR_LIB_DIR);
require_once 'Cache/Lite.php';

define('LANG_JP', 'jp');
define('LANG_EN', 'en');
define('DEFAULT_LANG', LANG_JP);

define('METRO_API_URL', 'https://api.tokyometroapp.jp/api/v2/');
define('METRO_API_KEY', '0b68431d7dd4827fe7aa6b16e89889c99112feafede2dee9ff9e933b8d237292');
define('METRO_API_TYPE_DATAPOINTS', 'datapoints');
define('METRO_API_TYPE_PLACES', 'places');

date_default_timezone_set('Asia/Tokyo');
ini_set('display_errors', TRUE);
ini_set("error_log", LOG_DIR . 'error_log');

define('DEFAULT_RADIUS', 500);

define('SITUATION_MORESOU'             , 0);
define('SITUATION_INSIDE_GATE'         , 1);
define('SITUATION_OUTSIDE_GATE'        , 2);
define('SITUATION_BABY_CHANGING_TABLE' , 3);
define('SITUATION_BABY_CHAIR'          , 4);
define('SITUATION_WHEELCHAIR_ASSESIBLE', 5);
define('SITUATION_TOILET_FOR_OSTOMATE' , 6);

define('SITUATION_MAX_STATIONS', 184);

