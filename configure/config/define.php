<?php
require_once 'env.php';

switch (ENV) {
    case ENV_DEVELOPMENT :
        define('BASE_DIR', '/home/www/metro/');
        define("SMARTY_BASE_DIR", "/home/smarty/metro/");
        define('CACHE_BASE_DIR', '/home/cache/metro/');
        define("LOG_DIR", "/home/log/php/metro/");
        define('BASE_URL', 'http://192.168.33.10/');
        break;
    case ENV_PRODUCTION :
        define('BASE_DIR', '/home/www/metro/');
        define("SMARTY_BASE_DIR", "/home/smarty/metro/");
        define('CACHE_BASE_DIR', '/home/cache/metro/');
        define("LOG_DIR", "/home/log/php/metro/");
        define('BASE_URL', 'http://54.64.204.235/');
        break;
}
