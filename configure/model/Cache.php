<?php
/**
 * キャッシュに関わる処理を受け持つクラスを定義
 *
 * (使用例)
 * $cache = new Cache();
 * $data = $cache->getData('id01');
 * if(!$data){
 *     $cache->setData('id01', 'data01');
 * }
 *
 * @author gnavi11
 * @package gnavi11-metro
 * @version $Id$
 */
class Cache {

    /**
     * 「Cache_Lite」のオプション設定
     *
     * @var array
     */
    private $_arrayOption = array();

    /**
     * 「Cache_Lite」のインスタンス
     *
     * @var object
     */
    private $_objectCache = NULL;

    /**
     * 「Cache_Lite」のオプション設定
     * PEAR エラーモード (raiseError がコールされた場合)
     *     CACHE_LITE_ERROR_RETURN：PEAR_Error オブジェクトを返す
     *     CACHE_LITE_ERROR_DIE   ：スクリプトを即時停止
     *
     * @var integer
     */
    private $_pearErrorMode = CACHE_LITE_ERROR_RETURN;

    /**
     * 「Cache_Lite」のオプション設定
     *     TRUE：CACHE_LITE_ERROR_RETURN モードにおいて、save() メソッドが PEAR_Error オブジェクト返す
     *
     * @var boolean
     */
    private $_errorHandlingAPIBreak = TRUE;

    /**
     * コンストラクタ
     * Cache_Lite のインスタンスを生成
     *
     * @param   array  $arrayOverwriteOption   上書き及び追加するオプション設定
     */
    public function __construct($arrayOverwriteOption=array()) {

        $_arrayBaseOption = array(
            'cacheDir'                => CACHE_DIR
          , 'lifeTime'                => CACHE_LIFE_TIME
          , 'hashedDirectoryLevel'    => CACHE_DIR_LEVEL
          , 'fileNameProtection'      => CACHE_FILE_NAME_PROTECTION
          , 'automaticSerialization'  => CACHE_AUTO_SERIALIZATION
          , 'automaticCleaningFactor' => CACHE_AUTO_CLEANING_FACTOR
          , 'pearErrorMode'           => $this->_pearErrorMode
          , 'errorHandlingAPIBreak'   => $this->_errorHandlingAPIBreak
          , 'hashedDirectoryUmask'    => CACHE_DIR_UMASK
        );

        $this->_arrayOption = $this->_overwriteArray($_arrayBaseOption, $arrayOverwriteOption);

        $this->_objectCache = new Cache_Lite($this->_arrayOption);
    }

    /**
     * キャッシュデータを取得
     *
     * @param   string  $cacheID  キャッシュID
     *
     * @return  mixed   $_data    FALSE    ：異常
     *                            FALSE以外：取得したデータ
     */
    public function getData($cacheID) {

        if (strlen($cacheID) >= 1) {
            $_data = $this->_objectCache->get($cacheID);
        } else {
            $_data = FALSE;
        }
        return $_data;
    }

    /**
     * キャッシュデータをセット
     *
     * @param   string  $cacheID  キャッシュID
     * @param   mixed   $data     保存するデータ
     *
     * @return  boolean $_result  TRUE ：正常
     *                            FALSE：異常
     */
    public function setData($cacheID, $data) {

        if (strlen($cacheID) >= 1) {
            $_result = $this->_objectCache->save($data, $cacheID);
            if ($_result !== TRUE) {
                $_pearMessage = "";
                if (is_object($_result) === TRUE) {
                    $_pearMessage = " " . $_result->getMessage();
                }
                $_message = "キャッシュデータのsetに失敗しました。cacheID = " . $cacheID . $_pearMessage;
                error_log($_message, 0);
    
                $_result = FALSE;
            }
        } else {
            $_message = "cacheID が指定されていません。";
            error_log($_message, 0);

            $_result = FALSE;
        }

        return $_result;
    }

    /**
     * 連想配列を上書き
     *
     * @param   array  $arrayBase       基本となる連想配列
     * @param   array  $arrayOverwrite  上書きする連想配列
     *
     * @return  array  $_arrayResult     上書き処理された連想配列
     */
    private function _overwriteArray($arrayBase, $arrayOverwrite) {

        $_arrayResult = $arrayBase;

        if (count($arrayOverwrite) >= 1) {
            foreach ($arrayOverwrite as $_key => $_value) {
                if (is_null($_value) === TRUE) {
                    unset($_arrayResult[$_key]);
                } else {
                    $_arrayResult[$_key] = $_value;
                }
            }
        }
        return $_arrayResult;
    }

}
