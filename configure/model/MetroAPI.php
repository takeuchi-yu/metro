<?php
/**
 * 東京メトロ API の Wrap クラス
 * 使用例1）パラメータで絞り込む場合
 * <pre>
 * $params = array();
 * $params['rdf:type'] = 'odpt:Station';
 * $params['dc:title'] = '東京';
 * $api = new MetroAPI();
 * $metro_data = $api->setType(METRO_API_TYPE_DATAPOINTS)->getData($params);
 * var_dump($metro_data);
 * </pre>
 * 使用例2）ID で絞り込む場合
 * <pre>
 * $id = 'urn:ucode:_00001C000000000000010000030C4406';
 * $api = new MetroAPI();
 * $metro_data = $api->setType(METRO_API_TYPE_PLACES)->getData(array(), $id);
 * echo $metro_data[0]->{'dc:title'}
 * </pre>
 *
 * @author gnavi11
 * @package gnavi11-metro
 * @version $Id$
 */
class MetroAPI {

    /**
     * API のタイプ
     *
     * @var string
     */
    private $type;

    /**
     * API タイプの設定
     *
     * @param string $type METRO_API_TYPE_DATAPOINTS または METRO_API_TYPE_PLACES
     * @return MetroAPI
     */
    public function setType($type){
        if ($type == METRO_API_TYPE_DATAPOINTS || $type == METRO_API_TYPE_PLACES) {
            $this->type = $type;
        }

        return $this;
    }

    /**
     * API タイプの取得
     *
     * @return string METRO_API_TYPE_DATAPOINTS または METRO_API_TYPE_PLACES
     */
    public function getType(){
        return $this->type;
    }

    /**
     * API に問い合わせたデータを取得する
     * 失敗した場合は FALSE を返します
     *
     * @param array $params 認証キー以外の情報を連想配列で渡してください。必要がなければ array() を渡してください。
     * @param string $id 任意の引数です。urn:ucode:_ または odpt から始まる ID で取得したい場合に指定してください。例）urn:ucode:_00001C000000000000010000030C4406
     * @return string
     */
    public function getData($params, $id = ''){
        $url = METRO_API_URL . $this->type;
        if ($id <> '') {
            $url .= '/' . $id;
        }
        $url .= '?';
        $params['acl:consumerKey'] = METRO_API_KEY;

        $encoded_params = array();
        foreach ($params as $key => $value) {
            $encoded_params[] = $key . '=' . $value;
        }

        $url .= implode('&', $encoded_params);

        $result = json_decode(file_get_contents($url));

        return $result;
    }
}