<?php
/**
 * 現在地座標取得 クラス
 *
 * 使用例)
 * $geo = new GeoLocation();
 * var_dump($geo->here);
 *
 * 返却値)
 *  array(
 *      'lt' => 35.621803899999996
 *     ,'ln' => 139.72560719999998
 *  )
 *  取得できなかった場合 
 *  array()
 * 
 * @author gnavi11
 * @package gnavi11-metro
 * @version $Id$
 */
class GeoLocation {

    /**
     * 日本の座標範囲
     */
    const MAX_LT = 45;
    const MIN_LT = 20;
    const MAX_LN = 153;
    const MIN_LN = 122;

    /**
     * 現在地座標
     *
     * @var array
     */
    public $here = array();

    /**
     * コンストラクタ
     *
     * @param  none
     * @return none
     */
    public function __construct(){

        if (isset($_GET['lt']) && $_GET['lt'] < self::MAX_LT && $_GET['lt'] > self::MIN_LT
         && isset($_GET['ln']) && $_GET['ln'] < self::MAX_LN && $_GET['ln'] > self::MIN_LN) {
            $this->here['lt'] = (float)$_GET['lt'];
            $this->here['ln'] = (float)$_GET['ln'];
        }
    }

}
