<?php
/**
 * Line コントローラー
 *
 * @author gnavi11
 * @package gnavi11-metro
 * @version $Id$
 */
require_once 'Base_Controller.php';
class Line_Controller extends Base_Controller {

    /**
     * メイン処理
     *
     * @return Line_Controller
     */
    public function main(){

        // メトロの全路線名を定義
        $metroRailWays = array("Ginza", "Marunouchi", "Hibiya", "Tozai", "Chiyoda", "Yurakucho", "Hanzomon", "Namboku", "Fukutoshin");
        $railWay = isset($_GET['rail_way']) ? $_GET['rail_way'] : null; //rail_wayのGETパラメーター取得

        // GETパラメータがメトロの路線名と一致していなければTOPにリダイレクト
        if(!in_array($railWay, $metroRailWays)) {
            header("Location: ".BASE_URL);
            exit;
        }

        $cache = new Cache();
        $cacheStationFacilityInfo = $cache->getData($railWay);
        // キャッシュからデータを取得できなかった場合APIより取得
        if(!$cacheStationFacilityInfo) {
            $stationInfo = $this->getStationInfo($railWay);

            $this->redirectCheck($stationInfo);
            ksort($stationInfo[$railWay]);
            $stationFacility = $this->getStationFacilityInfo($stationInfo, $railWay);
            $mergeStationFacility = $this->mergeStationFacilitySpec($stationFacility, $railWay);
            $stationFacilityInfo = $this->stationFacilityInfoSetEmpty($mergeStationFacility, $railWay);
            $cache->setData($railWay, $stationFacilityInfo);
            $cacheStationFacilityInfo = $stationFacilityInfo;
        }

        $data = array();
        $data['stationFacilityInfo'] = $cacheStationFacilityInfo;
        $this->assignVals($data)->display('line.tpl');

        return $this;
    }

    /**
     * 渡されたvalueが空の場合TOPにリダイレクト
     *
     * @param $value
     * @return
     */
    private function redirectCheck($value) {
        if(empty($value)) {
            header("Location: ".BASE_URL);
            exit;
        }
    }

    /**
     * GETパラメータのrail_wayからその路線に紐づいた駅情報を取得する
     *
     * @param string $railWay 路線名
     * @return array 駅情報のリスト
     * 例
     * array(1) {
     *   ["Ginza"]=> array(19) {
     *     ["G01"]=> array(2) {
     *       ["name"]=> string(6) "渋谷"
     *       ["key"]=> string(7) "Shibuya"
     *     }
     *     ["G02"]=>
     *     ・
     * 　　・
     *   }
     * }
     */
    private function getStationInfo($railWay) {

        $api = new MetroAPI();
        $odptRailWay="odpt.Railway:TokyoMetro." . $railWay;

        $params = array();
        $params['rdf:type'] = "odpt:Station";
        $params['odpt:railway'] = $odptRailWay;

        $metro_data = $api->setType(METRO_API_TYPE_DATAPOINTS)->getData($params);

        $stationInfo = array();
        foreach($metro_data as $value) {
            $stationInfo[$railWay][$value->{"odpt:stationCode"}]["name"] = $value->{"dc:title"};
            $stationInfo[$railWay][$value->{"odpt:stationCode"}]["key"] = preg_replace('/^odpt.Station:TokyoMetro.'. $railWay .'.*\./', '', $value->{'owl:sameAs'});
        }

        return $stationInfo;
    }

    /**
     * 駅の施設情報をAPIから取得して駅情報に施設情報を加えて返す
     * 存在しない施設情報は何もセットしない
     *
     * @param arrary $stationInfo 駅情報
     * @param string $railWay 路線名
     * @return array 駅情報に施設情報を加えたリスト
     * 例
     * array(1) {
     *   ["Ginza"]=> array(19) {
     *     ["G01"]=> array(4) {
     *       ["name"]=> string(9) "田原町"
     *       ["key"]=> string(11) "Tawaramachi"
     *       ["stationFacility"]=> array(5) {
     *         [0]=> array(3) {
     *           ["placeName"]=> string(28) "1番線ホーム（中央）"
     *           ["locate:in"]=> 1
     *           ["spac:WheelchairAssesible"]=> 1
     *           ["ug:BabyChair"]=>  1
     *           ["ug:BabyChangingTable"]=> 1
     *           ["ug:ToiletForOstomate"]=> 1
     *           }
     *         }
     *         [1]=> array(3) {
     *           ["placeName"]=> string(30) "２番線ホーム（中央）"
     *           ["locate:in"]=> 1
     *           ["spac:WheelchairAssesible"]=> 1
     *           ["ug:BabyChair"]=> 1
     *           }
     *         }
     *     }
     *     ["G02"]=> array(4) {
     *      ・
     *      ・
     *     }
     *   }
     * }
     */
    private function getStationFacilityInfo($stationInfo, $railWay) {

        foreach ($stationInfo[$railWay] as $key => $value) {
            $api = new MetroAPI();
            $params = array();
            $params['rdf:type'] = "odpt:StationFacility";
            $params['owl:sameAs'] = "odpt.StationFacility:TokyoMetro." . $value["key"];
            // 東京メトロAPIコール503対策 ５秒間隔をあける...
            sleep(2);
            $metro_data = $api->setType(METRO_API_TYPE_DATAPOINTS)->getData($params);

            $i = 0;
            $stationFacilityInfo[$railWay][$key] = $value;
            foreach ($metro_data[0]->{"odpt:barrierfreeFacility"} as $subValue) {

                $pattern = "/^odpt.StationFacility:TokyoMetro." . $railWay . "/";

                if($subValue->{"ugsrv:categoryName"} == "トイレ" && preg_match($pattern, $subValue->{"owl:sameAs"})) {

                    $stationFacilityInfo[$railWay][$key]["facilitySpec"][$i]["placeName"] = $subValue->{"odpt:placeName"};

                    // 改札内か改札外かの判定
                    if($subValue->{"odpt:locatedAreaName"} == "改札内") {
                        $stationFacilityInfo[$railWay][$key]["facilitySpec"][$i]["locate:in"] = 1;
                    }else{
                        $stationFacilityInfo[$railWay][$key]["facilitySpec"][$i]["locate:out"] = 1;
                    }

                    // 設備情報を配列にセット
                    if(is_array($subValue->{"spac:hasAssistant"})) {
                      foreach($subValue->{"spac:hasAssistant"} as $spec){
                          $stationFacilityInfo[$railWay][$key]["facilitySpec"][$i][$spec] = 1;
                      }
                    }

                    $i ++;
                }
            }
        }
        return $stationFacilityInfo;
    }

    /**
     * その駅に施設情報が複数存在する場合mergeした情報を追加して返す
     *
     * @param arrary stationFacilityInfo 駅情報
     * @param string railway 路線名
     * @return array
     */
    private function mergeStationFacilitySpec($stationFacilityInfo, $railWay) {

        foreach ($stationFacilityInfo[$railWay] as $key => $value) {
            $mergeSpec = array();
            if(isset($value["facilitySpec"])) {
                foreach($value["facilitySpec"] as $facilitySpec){
                    $mergeSpec = $mergeSpec + $facilitySpec;
                }
                unset($mergeSpec['placeName']);
                $stationFacilityInfo[$railWay][$key]["mergeSpec"] = $mergeSpec;
            }else{
                $stationFacilityInfo[$railWay][$key]["mergeSpec"] = $mergeSpec;
            }
        }
        return $stationFacilityInfo;
    }

    /**
     * 施設情報が存在しなければ0をセットする
     *
     * @param arrary stationFacilityInfo 駅情報
     * @param string railway 路線名
     * @return array
     */
    private function stationFacilityInfoSetEmpty($stationFacilityInfo, $railWay) {
      foreach($stationFacilityInfo[$railWay] as $key => $value) {
        if(isset($value["facilitySpec"])) {
          foreach($value["facilitySpec"] as $id => $facilitySpec){
            if(!isset($value["facilitySpec"][$id]["ug:BabyChangingTable"])) {
              $stationFacilityInfo[$railWay][$key]["facilitySpec"][$id]["ug:BabyChangingTable"] = 0;
            }
            if(!isset($value["facilitySpec"][$id]["ug:BabyChair"])) {
              $stationFacilityInfo[$railWay][$key]["facilitySpec"][$id]["ug:BabyChair"] = 0;
            }
            if(!isset($value["facilitySpec"][$id]["spac:WheelchairAssesible"])) {
              $stationFacilityInfo[$railWay][$key]["facilitySpec"][$id]["spac:WheelchairAssesible"] = 0;
            }
            if(!isset($value["facilitySpec"][$id]["ug:ToiletForOstomate"])) {
              $stationFacilityInfo[$railWay][$key]["facilitySpec"][$id]["ug:ToiletForOstomate"] = 0;
            }
            if(!isset($value["facilitySpec"][$id]["locate:out"])) {
              $stationFacilityInfo[$railWay][$key]["facilitySpec"][$id]["locate:out"] = 0;
            }
            if(!isset($value["facilitySpec"][$id]["locate:in"])) {
              $stationFacilityInfo[$railWay][$key]["facilitySpec"][$id]["locate:in"] = 0;
            }
          }
        }
        if(!empty($value["mergeSpec"]) ) {
          if(!isset($value["mergeSpec"]["ug:BabyChangingTable"])) {
            $stationFacilityInfo[$railWay][$key]["mergeSpec"]["ug:BabyChangingTable"] = 0;
          }
          if(!isset($value["mergeSpec"]["ug:BabyChair"])) {
            $stationFacilityInfo[$railWay][$key]["mergeSpec"]["ug:BabyChair"] = 0;
          }
          if(!isset($value["mergeSpec"]["spac:WheelchairAssesible"])) {
            $stationFacilityInfo[$railWay][$key]["mergeSpec"]["spac:WheelchairAssesible"] = 0;
          }
          if(!isset($value["mergeSpec"]["ug:ToiletForOstomate"])) {
            $stationFacilityInfo[$railWay][$key]["mergeSpec"]["ug:ToiletForOstomate"] = 0;
          }
          if(!isset($value["mergeSpec"]["locate:out"])) {
            $stationFacilityInfo[$railWay][$key]["mergeSpec"]["locate:out"] = 0;
          }
          if(!isset($value["mergeSpec"]["locate:in"])) {
            $stationFacilityInfo[$railWay][$key]["mergeSpec"]["locate:in"] = 0;
          }
        }

      }
      return $stationFacilityInfo;
    }

}
