<?php
/**
 * Error コントローラー
 *
 * @author gnavi11
 * @package gnavi11-metro
 * @version $Id$
 */
require_once 'Base_Controller.php';
class Error_Controller extends Base_Controller {

    /**
     * メイン処理
     *
     * @return Error_Controller
     */
    public function main(){
        $data = array();

        $this->assignVals($data)->display('error.tpl');

        return $this;
    }
}
