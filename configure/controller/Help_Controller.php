<?php
/**
 * Help コントローラー
 *
 * @author gnavi11
 * @package gnavi11-metro
 * @version $Id$
 */
require_once 'Base_Controller.php';
class Help_Controller extends Base_Controller {

    /**
     * メイン処理
     *
     * @return Help_Controller
     */
    public function main(){
        $data = array();

        $this->assignVals($data)->display('help.tpl');

        return $this;
    }
}
