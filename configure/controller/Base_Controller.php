<?php
/**
 * Base コントローラー
 *
 * @author gnavi11
 * @package gnavi11-metro
 * @version $Id$
 */
require_once '../configure/config/config.php';
require_once MODEL_DIR . 'MetroAPI.php';
require_once MODEL_DIR . 'GeoLocation.php';
require_once MODEL_DIR . 'Cache.php';

class Base_Controller {
    /**
     * Smarty インスタンス
     *
     * @var smarty
     */
    protected $smarty;

    /**
     * 初期化処理
     * Smarty のインスタンスの作成と初期設定をします
     */
    public function __construct(){
        $this->smarty = new Smarty();
        $this->smarty->template_dir = VIEW_DIR;
        $this->smarty->compile_dir = SMARTY_COMPILE_DIR;
        $this->smarty->cache_dir = SMARTY_CACHE_DIR;

    }

    /**
     * Smarty に変数を一括で assign します
     *
     * @param array $vals 変数名を key、値を value にした、連想配列で渡してください
     * @return Base_Controller
     */
    protected function assignVals($vals){
        foreach ($vals as $key => $value) {
            $this->smarty->assign($key, $value);
        }

        return $this;
    }

    /**
     * Smarty の display を呼びます
     *
     * @param string $template_name 'xxx.tpl' のようにテンプレートファイル名を渡してください
     * @return Base_Controller
     */
    protected function display($template_name){
        $this->smarty->display($template_name);
        return $this;
    }

    /**
     * API で取得した owl:sameAs の値から、英語名を取得する
     *
     * @param string $string API で取得した owl:sameAs の値
     * @return string
     */
    protected function getEnglishLineName($string) {
        $result = '';

        if ($string <> '') {
            $exploded_string = explode('.', $string);
            $result = $exploded_string[2];
        }

        return $result;
    }

    /**
     * 英語名からクラスの1文字を取得する
     * 半蔵門線の場合はzを、それ以外は頭文字を返します
     *
     * @param string $english_line_name 路線の英語名
     * @return string
     */
    protected function getLineClass($english_line_name) {
        $result = ($english_line_name == '') ? '' : strtolower(substr($english_line_name, 0, 1));

        if ($english_line_name == 'Hanzomon') {
            $result = 'z';
        }

        return $result;
    }
}
