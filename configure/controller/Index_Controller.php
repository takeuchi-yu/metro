<?php
/**
 * Index コントローラー
 *
 * @author gnavi11
 * @package gnavi11-metro
 * @version $Id$
 */
require_once 'Base_Controller.php';
class Index_Controller extends Base_Controller {

    private $lon;
    private $lat;

    /**
     * メイン処理
     *
     * @return Index_Controller
     */
    public function main(){
        $data = array();

        $nearest_line = $this->getNearestLine();
        $favorite_line = $this->getFavoriteLine();
        $line_list = $this->getLineList();
        $data['lines'] = $this->mergeLines($line_list, $nearest_line, $favorite_line);
        $data['lon'] = $this->lon;
        $data['lat'] = $this->lat;

        $this->assignVals($data)->display('index.tpl');

        return $this;
    }

    /**
     * 現在地から最も近い路線1つのクラスを取得する
     * 位置情報が取得できない、またはデフォルト半径以内に路線が見つからない場合は空文字を返します
     *
     * @return string
     */
    private function getNearestLine() {
        $result = '';

        $geo = new GeoLocation();
        $location = $geo->here;

        if ($location == array()) {
            return $result;
        }
        $this->lon = $location['ln'];
        $this->lat = $location['lt'];

        $api = new MetroAPI();
        $params = array();
        $params['rdf:type'] = 'odpt:Railway';
        $params['lon'] = $location['ln'];
        $params['lat'] = $location['lt'];
        $params['radius'] = DEFAULT_RADIUS;
        $metro_data = $api->setType(METRO_API_TYPE_PLACES)->getData($params);
        foreach ($metro_data as $line) {
            $name_en = $this->getEnglishLineName($line->{'owl:sameAs'});
            $result = $this->getLineClass($name_en);
            break;
        }

        return $result;
    }

    /**
     * Cookie からお気に入りの路線のクラスを取得する
     * お気に入りがなかった場合は空文字を返します
     *
     * @return array
     */
    private function getFavoriteLine() {
        if (!isset($_COOKIE['chika-toilet'])) {
            return '';
        }

        $cookie = $_COOKIE['chika-toilet'];
        return $this->getLineClass($cookie);
    }

    /**
     * 路線リストを取得する
     *
     * @return array
     */
    private function getLineList() {
        $result = array();

        $cache = new Cache();
        $cache_data = $cache->getData('line_list');

        if ($cache_data <> false) {
            return $cache_data;
        }

        $api = new MetroAPI();
        $params = array();
        $params['rdf:type'] = 'odpt:Railway';
        $line_data = $api->setType(METRO_API_TYPE_DATAPOINTS)->getData($params);

        foreach ($line_data as $line) {
            $data['name']['ja'] = $line->{'dc:title'};
            $data['name']['en'] = $this->getEnglishLineName($line->{'owl:sameAs'});
            if ($data['name']['en'] == 'MarunouchiBranch'){
                continue;
            }
            $data['class'] = $this->getLineClass($data['name']['en']);
            array_push($result, $data);
        }

        $cache->setData('line_list', $result);

        return $result;
    }

    /**
     * 最寄りとお気に入りの情報を路線リストにマージし、表示用にソートする
     *
     * @param array $line_list
     * @param string $nearest_line
     * @param string $favorite_line
     * @return array
     */
    private function mergeLines($line_list, $nearest_line, $favorite_line) {
        foreach ($line_list as $key => $line) {
            $line_list[$key]['nearest'] = 0;
            if ($line['class'] == $nearest_line) {
                $line_list[$key]['nearest'] = 1;
            }

            $line_list[$key]['favorite'] = 0;
            if ($line['class'] == $favorite_line) {
                $line_list[$key]['favorite'] = 1;
            }

        }

        usort($line_list, 'sortLinesForTop');

        return $line_list;
    }

}

/**
 * 与えられた配列を下記の順番にソート
 * 1. 最寄り
 * 2. お気に入り
 * 3. class 名の昇順
 *
 * @param array $a
 * @param array $b
 * @return number
 */
function sortLinesForTop($a, $b) {
    if ($b['nearest'] < $a['nearest']){
        return -1;
    }
    if ($b['nearest'] == $a['nearest'] && $b['favorite'] < $a['favorite']){
        return -1;
    }
    if ($b['nearest'] == $a['nearest'] && $b['favorite'] == $a['favorite'] && $a['class'] == $b['class']){
        return 0;
    }
    return ($b['nearest'] == $a['nearest'] && $b['favorite'] == $a['favorite'] && $a['class'] < $b['class']) ? -1 : 1;
}