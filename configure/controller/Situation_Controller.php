<?php
/**
 * Situation コントローラー.
 *
 * @author gnavi11
 * @package gnavi11-metro
 * @version $Id$
 */
require_once 'Base_Controller.php';
require_once MODEL_DIR.'MetroAPI.php';

class Situation_Controller extends Base_Controller {

    private $lat;
    private $lon;
    private $gps = 0;
    private $data = array();
    private $situation_title = array('0' => '今すぐ漏れそう',
                                     '1' => '改札の中にトイレあり',
                                     '2' => '改札の外にトイレあり',
                                     '3' => 'おむつ台あり',
                                     '4' => 'チャイルドトイレ',
                                     '5' => 'バリアフリートイレ',
                                     '6' => 'だれでもトイレ');
    private $situation_radius = array('50', '100', '200', '300', '500', '1000');

    /**
     * メイン処理
     *
     * @return Situation_Controller
     */
    public function main(){
        // ユーザ指定パラメータ
        $s_id   = isset($_GET['s']) ? urldecode($_GET['s']) : '';                     // 検索シチュエーションID
        $radius = (isset($_GET['r']) && $_GET['r'] > 0) ? urldecode($_GET['r']) : ''; // 検索範囲指定半径
        $radius = empty($radius) ? DEFAULT_RADIUS : $radius;
        // TODO 半径のバリデーション、最大半径何キロまで指定出来るようにする？

        if(!is_numeric($s_id) || !(($s_id === (String)SITUATION_MORESOU) ||
                                   ($s_id === (String)SITUATION_INSIDE_GATE) ||
                                   ($s_id === (String)SITUATION_OUTSIDE_GATE) ||
                                   ($s_id === (String)SITUATION_BABY_CHANGING_TABLE) ||
                                   ($s_id === (String)SITUATION_BABY_CHAIR) ||
                                   ($s_id === (String)SITUATION_WHEELCHAIR_ASSESIBLE) ||
                                   ($s_id === (String)SITUATION_TOILET_FOR_OSTOMATE))) {
            // TODO 確認：シチュエーションIDが指定されてない場合はTOPリダイレクトでいいよね
            header("Location: ".BASE_URL);
            exit;
        }

        // 現在地取得
        $geo = new GeoLocation();
        $points = $geo->here;

        if(!empty($points)) {
            $this->lat = $points['lt'];
            $this->lon = $points['ln'];

            // 近い駅取得
            $nearStations = $this->getNearStations($this->lat, $this->lon, $radius);

            // 施設情報取得
            $stationFacilities = $this->getStationFacility($nearStations);

            // 受け取ったシチュエーションIDの絞り込み条件に沿って絞り込み
            $filtered = $this->filterStationBySid($s_id, $stationFacilities);
            $data['count']   = count($filtered); // 絞り込み後の件数

            // 現在地に近い順にソート
            $sortedStations = $this->sortByLatAndLong($filtered);

            // 最大表示件数に絞り込み
            if(count($sortedStations) > SITUATION_MAX_STATIONS){
                array_splice($sortedStations, SITUATION_MAX_STATIONS);
            }

            $this->gps = 1;
            $data['data'] = $sortedStations; // 近い順ソート済み駅施設情報リスト
        }

        $data['title']   = $this->situation_title[$s_id];
        $data['gps']     = $this->gps;
        $data['url_top'] = BASE_URL;
        $data['url_own'] = BASE_URL.'?type='.TYPE_SITUATION.'&s='.$s_id.'&lt='.$this->lat.'&ln='.$this->lon;

        $data['radius']      = $radius;
        $data['radius_list'] = $this->situation_radius;

        $data['gnavi11'] = isset($_GET['mahoushojo']) ? 1 : 0;; // みんなオタクにな〜れ！(*^〇^*)

        $this->assignVals($data)->display('situation.tpl');

        return $this;
    }

    /**
     * 緯度経度を中心に指定半径内の駅全てを取得
     * @param int $lat 緯度
     * @param int $lon 経度
     * @param string $radius 半径
     * @return array 駅のリスト
     */
    protected function getNearStations($lat, $lon, $radius){
        $params = array();
        $params['rdf:type'] = 'odpt:Station';
        $params['lat']      = $lat;
        $params['lon']      = $lon;
        $params['radius']   = $radius;

        $api = new MetroAPI();
        $metro_data = $api->setType(METRO_API_TYPE_PLACES)->getData($params);

        $station_list = array();
        foreach ($metro_data as $id => $data) {
            // メトロの駅以外興味ない
            if($data->{'odpt:operator'} !== 'odpt.Operator:TokyoMetro') {
                continue;
            }
            // よけいな部分を削除
            $name = preg_replace('/odpt.Station:/', '', $data->{'owl:sameAs'});

            $station_list[$name]['geo:long']      = $data->{'geo:long'};
            $station_list[$name]['geo:lat']       = $data->{'geo:lat'};
            $station_list[$name]['odpt:facility'] = $data->{'odpt:facility'};
            $station_list[$name]['dc:title']      = $data->{'dc:title'};
        }
        return $station_list;
    }

    /**
     * 駅の施設情報を取得
     * @param array $list 駅のリスト
     * @return array 駅の施設情報(categoryNameがトイレの情報のみ)
     */
    protected function getStationFacility($list){
        $facility_list = array();

        $cache = new Cache();

        foreach ($list as $id => $data) {
            $tmp = '';
            $tmp = $cache->getData($id);

            // キャッシュない場合は、APIからデータ取得してごにょごにょする
            if(!$tmp) {
                $api = new MetroAPI();
                $res = array();
                $res = $api->setType(METRO_API_TYPE_DATAPOINTS)->getData(array(), $data['odpt:facility']);

                // 駅施設情報から、"対象の沿線"かつ"ug:Toilet" の情報だけに絞り込む
                $info = array();
                foreach($res[0]->{'odpt:barrierfreeFacility'} as $i => $detail) {
                    if($detail->{'@type'} == 'ug:Toilet' && strstr($detail->{'owl:sameAs'}, 'odpt.StationFacility:'.$id)) {
                        $info[$detail->{'owl:sameAs'}] = $detail;
                    }
                }

                // トイレがない場合だってあるのさ
                if(!empty($info)) {
                    $formated = $this->facilityArrayToStructuredData($id, $data, $info);

                    $cache->setData($id, $formated); // ここまで構造化してからキャッシュ!
                    $facility_list[$id] = $formated;
                }
            } else {
                $facility_list[$id] = $tmp;
            }
        }
        return $facility_list;
    }

    /**
     * APIから取得した情報をキャッシュ用のデータ構造に整形する
     * @param array $id id(駅名)
     * @param array $station 駅情報
     * @param array $facility 駅施設情報
     * @return array 整形した駅情報
     */
    protected function facilityArrayToStructuredData($id, $station, $facility){
        $formated = array();

        $formated['name']['ja'] = $station['dc:title'];
        $formated['name']['en'] = $this->getEnglishLineName($id);
        $formated['name']['id'] = $id;
        $formated['geo']['lon'] = $station['geo:long'];
        $formated['geo']['lat'] = $station['geo:lat'];

        $line = explode('.', $id);
        $formated['line'] = $this->getLineClass($line[1]);

        $j = 0;
        foreach($facility as $i => $data) {
            $facility_array = $data->{'spac:hasAssistant'};
            //$key = preg_replace('/odpt.StationFacility:/', '', $i);

            $formated['toilet'][$j]['flags']['bct'] = in_array('ug:BabyChangingTable',     $facility_array) ? 1 : 0;
            $formated['toilet'][$j]['flags']['bc']  = in_array('ug:BabyChair',             $facility_array) ? 1 : 0;
            $formated['toilet'][$j]['flags']['wa']  = in_array('spac:WheelchairAssesible', $facility_array) ? 1 : 0;
            $formated['toilet'][$j]['flags']['tfo'] = in_array('ug:ToiletForOstomate',     $facility_array) ? 1 : 0;

            $formated['toilet'][$j]['locate']['inOrOut'] = $data->{'odpt:locatedAreaName'};
            $formated['toilet'][$j]['locate']['detail']  = $data->{'odpt:placeName'};

            $formated['toilet'][$j]['key'] = preg_replace('/odpt.StationFacility:/', '', $i);

            $j++;
        }
        return $formated;
    }

    /**
     * 指定のシチュエーションに合う駅のみに絞り込み
     * @param string $s_id シチュエーションID
     * @param array  $list 施設情報付き駅リスト
     * @return array 条件にあう施設情報付き駅リスト
     */
    private function filterStationBySid($s_id, $list){
        $filtered = array();
        if($s_id == SITUATION_MORESOU) { // 今すぐ漏れそう(複合条件ないのでそのまま)
            $filtered = $list;
        } elseif($s_id == SITUATION_BABY_CHANGING_TABLE) {  // おむつ台(spac:hasAssistant に"ug:BabyChangingTable"があるか)
            $filtered = $this->filterStationsByFlags("bct", $list);
        } elseif($s_id == SITUATION_BABY_CHAIR) {           // チャイルドトイレ(spac:hasAssistant に"ug:BabyChair"があるか)
            $filtered = $this->filterStationsByFlags("bc", $list);
        } elseif($s_id == SITUATION_WHEELCHAIR_ASSESIBLE) { // バリアフリートイレ(spac:hasAssistant に"spac:WheelchairAssesible"があるか)
            $filtered = $this->filterStationsByFlags("wa", $list);
        } elseif($s_id == SITUATION_TOILET_FOR_OSTOMATE) {  // だれでもトイレ(spac:hasAssistant に"ug:ToiletForOstomate"があるか)
            $filtered = $this->filterStationsByFlags("tfo", $list);
        } elseif($s_id == SITUATION_INSIDE_GATE) {          // 改札内(odpt:locatedAreaNameが"改札内")
            $filtered = $this->filterStationsByLocate("改札内", $list);
        } elseif($s_id == SITUATION_OUTSIDE_GATE) {         // 改札外(odpt:locatedAreaNameが"改札外")
            $filtered = $this->filterStationsByLocate("改札外", $list);
        } else {
            throw new Exception('不正なシチュエーション');
        }
        return $filtered;
    }

    /**
     * 指定のトイレ設備フラグを持つ駅のみに絞り込み
     * @param string $flag トイレ設備フラグkey
     * @param array  $list 施設情報付き駅リスト
     * @return array 条件にあう施設情報付き駅リスト
     */
    private function filterStationsByFlags($flag, $list){
        $res = array();
        foreach($list as $i => $data) {
            foreach($data['toilet'] as $j => $toilet_data) {
                if($toilet_data['flags'][$flag] == 1) {
                    $res[$i] = $data;
                    break;
                }
            }
        }
        return $res;
    }

    /**
     * 指定の場所(改札内/改札外)にトイレがある駅のみに絞り込み
     * @param string $flag 改札内/改札外
     * @param array  $list 施設情報付き駅リスト
     * @return array 条件にあう施設情報付き駅リスト
     */
    private function filterStationsByLocate($flag, $list){
        $res = array();
        foreach($list as $i => $data) {
            foreach($data['toilet'] as $j => $toilet_data) {
                if($toilet_data['locate']['inOrOut'] == $flag) {
                    $res[$i] = $data;
                    break;
                }
            }
        }
        return $res;
    }

    /**
     * 現在地の緯度経度に近い駅順にソート
     * @param array  $list 施設情報付き駅リスト
     * @return array ソート済み施設情報付き駅リスト
     */
    private function sortByLatAndLong($list){
        // 現在地との距離を追加
        $this->addDistance($this->lat, $this->lon, $list);

        usort($list, "sortByDistance");
        return $list;
    }

    /**
     * 指定の緯度経度との距離を連想配列の要素に追加する
     * @param array $list
     * @return array 現在地間の距離を追加した駅リスト
     */
    private function addDistance($lat, $lon, &$list){
        foreach($list as &$data){
            $data['distance'] = $this->location_distance($lat, $lon, $data['geo']['lat'], $data['geo']['lon']);
        }
    }

    /**
     * 緯度経度を用いて２点間の直線距離を求める（世界測地系）
     * http://kudakurage.hatenadiary.com/entry/20100319/1268986000
     * @param string $lat1 A地点の緯度
     * @param string $lon1 A地点の経度
     * @param string $lat2 B地点の緯度
     * @param string $lon2 B地点の経度
     * @return array 2点間の直線距離
     */
    private function location_distance($lat1, $lon1, $lat2, $lon2){
        $lat_average    = deg2rad($lat1 + (($lat2 - $lat1) / 2)); //２点の緯度の平均
        $lat_difference = deg2rad($lat1 - $lat2);                 //２点の緯度差
        $lon_difference = deg2rad($lon1 - $lon2);                 //２点の経度差
        $curvature_radius_tmp = 1 - 0.00669438 * pow(sin($lat_average), 2);
        $meridian_curvature_radius              = 6335439.327 / sqrt(pow($curvature_radius_tmp, 3)); //子午線曲率半径
        $prime_vertical_circle_curvature_radius = 6378137 / sqrt($curvature_radius_tmp);             //卯酉線曲率半径

        //２点間の距離
        $distance = pow($meridian_curvature_radius * $lat_difference, 2) + pow($prime_vertical_circle_curvature_radius * cos($lat_average) * $lon_difference, 2);
        $distance = sqrt($distance);

        return $distance;
    }
}

// 配列のdistance要素の昇順にソート
function sortByDistance($a, $b){
    $res = 0;
    if($a['distance'] == $b['distance']){
        $res = 0;
    } else {
        $res = $a['distance'] > $b['distance'] ? 1 : -1;
    }
    return $res;
}

