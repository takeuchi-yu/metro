<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0,minimum-scale=1.0, maximum-scale=1.0,user-scalable=no">
<meta property="og:title" content="[ちかトイレくん]" />
<meta property="og:description" content="東京メトロ駅にあるトイレを現在地から近い順やシチュエーション別で検索することが出来るページです。" />
<meta property="og:url" content="http://54.64.204.235/" />
<meta property="og:type" content="website" />
<meta property="og:image" content="http://54.64.204.235/img/common/webClipIcon.png" />
<title>[ちかトイレくん]</title>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<!--▼UA判定用JS-->
<!--<script type="text/javascript" src="./js/get_ua.js"></script>-->
<script type="text/javascript" src="./js/geo_location.js"></script>
<script>getGeoFirst()</script>
<link rel="shortcut icon" href="img/common/favicon.png">
<link rel="apple-touch-icon" href="img/common/webClipIcon.png">
<link rel="stylesheet" type="text/css" href="./css/common.css">
<link rel="stylesheet" type="text/css" href="./css/style.css">
<link rel="stylesheet" type="text/css" href="./css/portrait_popup.css">
<link rel="stylesheet" type="text/css" href="./css/portrait_menu_top.css">
<link rel="stylesheet" type="text/css" href="./css/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />
<script type="text/javascript" src="./js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="./js/common.js"></script>
<script type="text/javascript" src="./js/menu.js"></script>
<script type="text/javascript" src="./js/popup.js"></script>
<!--[if lte IE 8]>
<script type="text/javascript" src="./js/jquery.backgroundSize.js"></script>
<script>
   $(function() {
   $(".pri_name").css( "background-size", "cover" );  //backgounrd-size:coverを使う場合//
   });
</script>
<![endif]-->
</head>

<body>
{include file='tag_manager.tpl'}
<div id="logo_area">
<h1>ちかトイレくん</h1>
</div>
{include file='menu.tpl'}
<div id="main">
<section>
<a href="?type=situation&s={$smarty.const.SITUATION_MORESOU}&ln={$lon}&lt={$lat}"><div id="pants"><img src="img/top/pants_tr_toilet_br.png"></div></a>
</section>

<section>
<div id="explain" onclick="obj=document.getElementById('explain_text').style; obj.display=(obj.display=='none')?'block':'none';">
<a style="cursor:pointer;"><div class="arrow"></div><span>&nbsp;ちかトイレくんとは？</span></a>
</div>
<div id="explain_text" style="display:none;clear:both;">
あなたが東京メトロに乗車している最中や東京メトロの駅近くにいる時、突然お腹がギュルギュル〜…今すぐトイレに行きたい！
このアプリはそんな胃腸の弱いあなたのお悩みを解決！東京メトロ駅にあるトイレを現在地から近い順やシチュエーション別で検索することが出来るアプリです。
</div>
</section>

<section>
<div id="face">
<div id="eyes">
<div id="line_status">
{foreach from=$lines key=key item=line}
{if $key <= 1}
<div class="line_information">{if $line['nearest']}<p>現在地</p>{/if}{if $line['favorite']}<p>お気に入り</p>{/if}</div>
{/if}
{/foreach}
</div>
{foreach from=$lines key=key item=line}
{if $key <= 1}
<div class="eyebox">
<div class="eye_line">
<a href="?type=line&rail_way={$line.name.en}"><div class="eyes_c cl_{$line.class}"><div class="eyes_inner_c"><div class="ltwh_{$line.class} pri_name"></div></div></div></div>
<p class="line_name">{$line.name.ja}線</p></a>
</div>
{/if}
{/foreach}
</div>

<div id="cheekbox">
<div class="cheek_l"><div class="cheek_inner"></div></div>
<div class="cheek_r"><div class="cheek_inner"></div></div>
</div>
<a id="popup" href="popup_val.html"><div id="mouth"></div></a>
</div>
</section>

<section>
<p class="menu_t t_line"><span>路線選択</span></p>
<div id="select_line">
<div class="select_line_icons">
<div class="lines">
<ul>
{foreach from=$lines key=key item=line}
{if $key > 1}
{if $key != 2 && ($key-2)%4 == 0}
</ul>
</div>
<div class="lines">
<ul>
<li><a href="?type=line&rail_way={$line.name.en}"><div class="line_c cl_{$line.class}"><div class="line_inner_c"><div class="ltblc_{$line.class} ord_name"></div></div></div><span class="line_name">{$line.name.ja}線</span></a></li>
{else}
<li><a href="?type=line&rail_way={$line.name.en}"><div class="line_c cl_{$line.class}"><div class="line_inner_c"><div class="ltblc_{$line.class} ord_name"></div></div></div><span class="line_name">{$line.name.ja}線</span></a></li>
{/if}
{/if}
{/foreach}
</ul>
</div>
</div>
</div>
</section>

<section>
<p class="menu_t t_sit"><span>シチュエーション選択</span></p>
<div id="situation_nav">
<ul>
<li class="available"><a href="?type=situation&s={$smarty.const.SITUATION_MORESOU}&ln={$lon}&lt={$lat}"><img src="img/train_map/theme_danger.png"><p>今すぐ漏れそう</p></a></li>
<li class="available"><a href="?type=situation&s={$smarty.const.SITUATION_INSIDE_GATE}&ln={$lon}&lt={$lat}"><img src="img/train_map/theme_in.png"><p>改札の中に<br />トイレあり</p></a></li>
<li class="available"><a href="?type=situation&s={$smarty.const.SITUATION_OUTSIDE_GATE}&ln={$lon}&lt={$lat}"><img src="img/train_map/theme_out.png"><p>改札の外に<br />トイレあり</p></a></li>
<li class="available"><a href="?type=situation&s={$smarty.const.SITUATION_BABY_CHANGING_TABLE}&ln={$lon}&lt={$lat}"><img src="img/train_map/theme_babyChangingTable.png"><p>ベビーシート<br />あり</p></a></li>
<li class="available"><a href="?type=situation&s={$smarty.const.SITUATION_BABY_CHAIR}&ln={$lon}&lt={$lat}"><img src="img/train_map/theme_babyChair.png"><p>ベビーチェア<br />あり</p></a></li>
<li class="available"><a href="?type=situation&s={$smarty.const.SITUATION_WHEELCHAIR_ASSESIBLE}&ln={$lon}&lt={$lat}"><img src="img/train_map/theme_wheelchair.png"><p>車いす対応<br />トイレ</p></a></li>
<li class="available"><a href="?type=situation&s={$smarty.const.SITUATION_TOILET_FOR_OSTOMATE}&ln={$lon}&lt={$lat}"><img src="img/train_map/theme_daredemo.png"><p>オストメイト<br />対応トイレ</p></a></li>
<li class="unavailable"><img src="img/top/unavailable.png"></li>
<li class="unavailable"><img src="img/top/unavailable.png"></li>
</ul>
</div>
</section>

<section>
<ul class="sns">
<li class="tweet"><a href="https://twitter.com/share" class="twitter-share-button" data-url="http://54.64.204.235/" data-lang="ja" data-count="none">Tweet</a></li>
<li class="fb-like" data-href="http://54.64.204.235/" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></li>
<li class="hatena"><a href="http://b.hatena.ne.jp/entry/http://54.64.204.235/" class="hatena-bookmark-button" data-hatena-bookmark-title="[ちかトイレくん]" data-hatena-bookmark-layout="standard-noballoon" data-hatena-bookmark-lang="ja" title="このエントリーをはてなブックマークに追加"><img src="https://b.st-hatena.com/images/entry-button/button-only@2x.png" alt="このエントリーをはてなブックマークに追加" width="20" height="20" style="border: none;" /></a></li>
</ul>
{literal}
<script type="text/javascript">!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
<div id="fb-root"></div>
<script type="text/javascript">(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script type="text/javascript" src="https://b.st-hatena.com/js/bookmark_button.js" charset="utf-8" async="async"></script>
{/literal}
</section>
<p id="copyright"><small>&copy;2014 イチイチ</small></p>
</div>
</body>
</html>
