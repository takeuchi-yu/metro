<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="user-scalable=no">
<title>[ちかトイレくん]路線図</title>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<!--▼UA判定用JS-->
<script type="text/javascript" src="./js/get_ua.js"></script>
<script type="text/javascript" src="./js/geo_location.js"></script>
<link rel="shortcut icon" href="img/common/favicon.png">
<link rel="apple-touch-icon" href="img/common/webClipIcon.png">
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="./css/common.css">
<link rel="stylesheet" type="text/css" href="./css/station_route.css">
<!-- 縦 -->
<link rel="stylesheet" media="all and (orientation:portrait)" href="./css/portrait_menu.css">
<!-- 横 -->
<link rel="stylesheet" media="all and (orientation:landscape)" href="./css/landscape_menu.css">
<script type="text/javascript" src="./js/route_station.js"></script>
<script type="text/javascript" src="./js/menu.js"></script>
<script type="text/javascript" src="./js/bookmark.js"></script>
</head>
<body>
{include file='tag_manager.tpl'}
        <header>
            {include file='menu.tpl'}
        </header>
        {foreach from=$stationFacilityInfo key=railWay item=v}
            <section>
                <div class="bookmark_box" onclick="setLineCookie('{$railWay}');">
                    <i class="fa fa-star-o fa-5x bookmark" id="bookmark_0"></i>
                </div>
                <script>displayBookmark('{$railWay}')</script>
                <div class="contents overflow">
                    <div class="main">
                    <ol>
                        {foreach from=$v key=stationCode item=station}
                            <li>
                                <div class="station_name_box">
                                    <img src="img/train_map/line_{{$stationCode|lower}|substr:0:1}.png">
                                    <p class="station_mark_number">{$stationCode|substr:1:2}</p>
                                </div>
                                <div class="station_info_box overflow">
                                    <span>
                                        <img src="img/common/arrow_left.png">
                                    </span>
                                    <h2 class="overflow">{$station["name"]}<br />
                                    <p>{$station["key"]}</p></h2>

                                    {if $station["mergeSpec"]==null}
                                        <div class="station_info_callout trigger">
                                            <div class="merge_station_info">
                                                <p class="station_info_callout_not"><img src="img/train_map/theme_in.png"></p>
                                                <p class="station_info_callout_not"><img src="img/train_map/theme_out.png"></p>
                                                <p class="station_info_callout_not"><img src="img/train_map/theme_babyChangingTable.png"></p>
                                                <p class="station_info_callout_not"><img src="img/train_map/theme_babyChair.png"></p>
                                                <p class="station_info_callout_not"><img src="img/train_map/theme_wheelchair.png"></p>
                                                <p class="station_info_callout_not"><img src="img/train_map/theme_daredemo.png"></p>
                                            </div>
                                        </div>
                                    {else}
                                        <div class="station_info_callout trigger" id="{$stationCode}">
                                            <div class="merge_station_info">
                                                <p {if $station["mergeSpec"]["locate:in"]==0} class="station_info_callout_not"{/if}><img src="img/train_map/theme_in.png"></p>
                                                <p {if $station["mergeSpec"]["locate:out"]==0} class="station_info_callout_not"{/if}><img src="img/train_map/theme_out.png"></p>
                                                <p {if $station["mergeSpec"]["ug:BabyChangingTable"]==0} class="station_info_callout_not"{/if}><img src="img/train_map/theme_babyChangingTable.png"></p>
                                                <p {if $station["mergeSpec"]["ug:BabyChair"]==0} class="station_info_callout_not"{/if}><img src="img/train_map/theme_babyChair.png"></p>
                                                <p {if $station["mergeSpec"]["spac:WheelchairAssesible"]==0} class="station_info_callout_not"{/if}><img src="img/train_map/theme_wheelchair.png"></p>
                                                <p {if $station["mergeSpec"]["ug:ToiletForOstomate"]==0} class="station_info_callout_not"{/if}><img src="img/train_map/theme_daredemo.png"></p>
                                            </div>
                                            {foreach from=$station["facilitySpec"] key=facilitySpecNumber item=facilitySpec}
                                                {if is_array($facilitySpec)}
                                                    <div class="acordion_tree overflow">
                                                        <div class="acordion_tree_station_info overflow">
                                                            <span>{$facilitySpec["placeName"]}</span>
                                                            <div class="info_icons overflow">
                                                                <p {if $facilitySpec["locate:in"]==0} class="station_info_callout_not"{/if}><img src="img/train_map/theme_in.png"></p>
                                                                <p {if $facilitySpec["locate:out"]==0} class="station_info_callout_not"{/if}><img src="img/train_map/theme_out.png"></p>
                                                                <p {if $facilitySpec["ug:BabyChangingTable"]==0} class="station_info_callout_not"{/if}><img src="img/train_map/theme_babyChangingTable.png"></p>
                                                                <p {if $facilitySpec["ug:BabyChair"]==0} class="station_info_callout_not"{/if}><img src="img/train_map/theme_babyChair.png"></p>
                                                                <p {if $facilitySpec["spac:WheelchairAssesible"]==0} class="station_info_callout_not"{/if}><img src="img/train_map/theme_wheelchair.png"></p>
                                                                <p {if $facilitySpec["ug:ToiletForOstomate"]==0} class="station_info_callout_not"{/if}><img src="img/train_map/theme_daredemo.png"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                {/if}
                                            {/foreach}
                                        </div>
                                    {/if}
                                </div>
                            </li>
                        {/foreach}
                    </ol>
                </div>
                <div class="route_boader" style="height:100%"></div>
            </section>
        {/foreach}
        <p id="copyright"><small>&copy;2014 イチイチ</small></p>
    </body>
</html>
