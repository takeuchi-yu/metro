<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0,minimum-scale=1.0, maximum-scale=1.0,user-scalable=no">
<title>[ちかトイレくん]シチュエーション検索結果</title>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<!--▼UA判定用JS-->
<script type="text/javascript" src="./js/get_ua.js"></script>
<script type="text/javascript" src="./js/geo_location.js"></script>
<link rel="shortcut icon" href="img/common/favicon.png">
<link rel="apple-touch-icon" href="img/common/webClipIcon.png">
<link rel="stylesheet" type="text/css" href="./css/common.css">
<link rel="stylesheet" type="text/css" href="./css/situation.css">
<!-- 縦 -->
<link rel="stylesheet" media="all and (orientation:portrait)" href="./css/portrait_menu_top.css">
<!-- 横 -->
<link rel="stylesheet" media="all and (orientation:landscape)" href="./css/landscape_menu.css">
<script type="text/javascript" src="./js/menu.js"></script>
<script type="text/javascript" src="./js/situation.js"></script>
</head>
<body>
{include file='tag_manager.tpl'}
{include file='menu.tpl'}
<div id="main">
<section>
<div id="situation_info">
<h1><span>{$title}</span><p>シチュエーション検索結果一覧</p></h1>
</div>
</section>
{if $gps === 1}
<section>
<p id="result_num"><span>{$count}駅&nbsp;HIT</span></p>

<div id="dropdown_area">
<p class="drop_arrow">▼</p>
<select class="dropdown">
{section name=i loop=$radius_list}
<option value="{$url_own}&r={$radius_list[i]}" {if $radius_list[i] eq $radius}selected{/if}>{$radius_list[i]}m以内</option>
{/section}
</select>
</div>

<div id="station_cassette">
{if $count > 0}
<ul>

{section name=i loop=$data}
<li>
<div class="station fadeInDown">
<div class="line_c cl_{$data[i].line}"><div class="line_inner_c"><div class="ltblc_{$data[i].line} ord_name"></div></div></div>
<div class="station_name">
<h2>{$data[i].name.ja}</h2>
<p>{$data[i].name.en}</p>
</div>
<p class="distance">現在地から{$data[i].distance|round:0}m</p>
<div class="station_info">
<div class="info_sign">
{section name=j loop=$data[i].toilet}
<div class="place_info">
<span>{$data[i].toilet[j].locate.detail}</span>
<div class="info_icons">
<p class="sit_in {if $data[i].toilet[j].locate.inOrOut != '改札内'}call_out{/if}"></p>
<p class="sit_out {if $data[i].toilet[j].locate.inOrOut != '改札外'}call_out{/if}"></p>
<p class="sit_baby {if $data[i].toilet[j].flags.bct == 0}call_out{/if}"></p>
<p class="sit_child {if $data[i].toilet[j].flags.bc == 0}call_out{/if}"></p>
<p class="sit_wheel {if $data[i].toilet[j].flags.wa == 0}call_out{/if}"></p>
<p class="sit_ostomate {if $data[i].toilet[j].flags.tfo == 0}call_out{/if}"></p>
</div>
{/section}
</div>
</div>
</div>
</div>
</li>
{/section}

</ul>
{else}
<div id="situation_error">
<p class="situation_error_txt">選択されたシチュエーションで<br />ヒットする駅はありませんでした。</p>
<a href="{$url_top}">TOPへ</a>
</div>
{/if}
</div>
</section>
{else}
<div id="situation_error">
<p class="situation_error_txt">位置情報を正常に取得出来ませんでした。<br />※ シチュエーション検索は位置情報機能がONの場合のみ利用出来ます。</p>
<a href="{$url_top}">TOPへ</a>
</div>
{/if}

</div>
{if $gnavi11 === 1}<p class="flesh">イチイチ同期よ永遠に―。</p>{/if}
<p id="copyright"><small>&copy;2014 イチイチ</small></p>
</body>
</html>
