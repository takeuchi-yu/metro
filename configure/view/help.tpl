<!DOCTYPE html>
<html lang="ja">
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="user-scalable=no">
        <title>[ちかトイレくん]HELP!!</title>
        <!--▼UA判定用JS-->
        <script type="text/javascript" src="./js/get_ua.js"></script>
        <link rel="shortcut icon" href="img/common/favicon.png">
        <link rel="apple-touch-icon" href="img/common/webClipIcon.png">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/common.css">
        <link rel="stylesheet" type="text/css" href="css/help.css">
        <!-- 縦 -->
        <link rel="stylesheet" media="all and (orientation:portrait)" href="css/portrait_menu.css">
        <!-- 横 -->
        <link rel="stylesheet" media="all and (orientation:landscape)" href="css/landscape_menu.css">
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="./css/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
        <script type="text/javascript" src="./js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
        <script type="text/javascript" src="js/help.js"></script>
        <script type="text/javascript" src="js/menu.js"></script>
    </head>
    <body>
        <header>
            <div class="pull_menu">
                <div class="pull_menu_button open">
                </div>
                <nav class="pull_menu_nav">

                    <div class="pull_menu_nav_list another_list">
                        <p class="menu_t t_sit"><span>menu</span></p>
                        <ul>
                            <li><a href="#">現在地情報再取得</a></li>
                            <li><a href="#">TOPページへ</a></li>
                            <li><a href="#">ヘルプ</a></li>
                        </ul>
                    </div>

                    <div class="pull_menu_nav_list situation_list">
                        <p class="menu_t t_sit situation_title"><span>シチュエーション選択</span></p>
                        <ul>
                            <li><a href="#"><img src="img/train_map/theme_danger.png"><span>今すぐ漏れそう</span></a></li>
                            <li><a href="#"><img src="img/train_map/theme_in.png"><span>改札の中にトイレあり</span></a></li>
                            <li><a href="#"><img src="img/train_map/theme_out.png"><span>改札の外にトイレあり</span></a></li>
                            <li><a href="#"><img src="img/train_map/theme_babyChangingTable.png"><span>ベビーシートあり</span></a></li>
                            <li><a href="#"><img src="img/train_map/theme_babyChair.png"><span>ベビーチェアあり</span></a></li>
                            <li><a href="#"><img src="img/train_map/theme_wheelchair.png"><span>車いす対応トイレ</span></a></li>
                            <li><a href="#"><img src="img/train_map/theme_daredemo.png"><span>オストメイトトイレ</span></a></li>
                        </ul>
                    </div>
                    <p class="pull_menu_nav_close close">閉じる</p>
                </nav>
            </div>
        </header>
        <section>
        <div class="main">
            <div class="white_back list" id="top">
                <h1>目次</h1>
                <ol>
                    <li><i class="fa fa-question-circle"></i><a href="#box_1">アプリの説明</a></li>
                    <li><i class="fa fa-question-circle"></i><a href="#box_2">TOP</a></li>
                    <li><i class="fa fa-question-circle"></i><a href="#box_3">路線図</a></li>
                    <li><i class="fa fa-question-circle"></i><a href="#box_4">シチュエーション</a></li>
                    <li><i class="fa fa-question-circle"></i><a href="#box_5">メニュー</a></li>
                </ol>
            </div>

            <div class="white_back help_box" id="box_1">
                <div class="help_box_img">
                    <a href="img/help/img1.jpg" id="help_img">
                        <img src="img/help/img1.jpg">
                    </a>
                </div>
                <div class="help_box_text">
                    <ol>
                        <li><i class="fa fa-lightbulb-o"></i>ほげほげほげほ</li>
                        <li><i class="fa fa-lightbulb-o"></i>ほげほげほげほ</li>
                        <li><i class="fa fa-lightbulb-o"></i>ほげほげほげほ</li>
                        <li><i class="fa fa-lightbulb-o"></i>ほげほげほげほ</li>
                        <li><a href="#top">ページ上部に戻る</a></li>
                    </ol>
                </div>
            </div>

            <div class="white_back help_box" id="box_2">
                <div class="help_box_img">
                    <a href="img/help/img2.jpg" id="help_img">
                        <img src="img/help/img2.jpg">
                    </a>
                </div>
                <div class="help_box_text">
                    <ol>
                        <li><i class="fa fa-lightbulb-o"></i>ほげほげほげほ</li>
                        <li><i class="fa fa-lightbulb-o"></i>ほげほげほげほ</li>
                        <li><i class="fa fa-lightbulb-o"></i>ほげほげほげほ</li>
                        <li><i class="fa fa-lightbulb-o"></i>ほげほげほげほ</li>
                        <li><a href="#top">ページ上部に戻る</a></li>
                    </ol>
                </div>
            </div>

            <div class="white_back help_box" id="box_3">
                <div class="help_box_img">
                    <a href="img/help/img3.jpg" id="help_img">
                        <img src="img/help/img3.jpg">
                    </a>
                </div>
                <div class="help_box_text">
                    <ol>
                        <li><i class="fa fa-lightbulb-o"></i>ほげほげほげほ</li>
                        <li><i class="fa fa-lightbulb-o"></i>ほげほげほげほ</li>
                        <li><i class="fa fa-lightbulb-o"></i>ほげほげほげほ</li>
                        <li><i class="fa fa-lightbulb-o"></i>ほげほげほげほ</li>
                        <li><a href="#top">ページ上部に戻る</a></li>
                    </ol>
                </div>
            </div>

            <div class="white_back help_box" id="box_4">
                <div class="help_box_img">
                    <a href="img/help/img4.jpg" id="help_img">
                        <img src="img/help/img4.jpg">
                    </a>
                </div>
                <div class="help_box_text">
                    <ol>
                        <li><i class="fa fa-lightbulb-o"></i>ほげほげほげほ</li>
                        <li><i class="fa fa-lightbulb-o"></i>ほげほげほげほ</li>
                        <li><i class="fa fa-lightbulb-o"></i>ほげほげほげほ</li>
                        <li><i class="fa fa-lightbulb-o"></i>ほげほげほげほ</li>
                        <li><a href="#top">ページ上部に戻る</a></li>
                    </ol>
                </div>
            </div>

            <div class="white_back help_box" id="box_5">
                <div class="help_box_img">
                    <a href="img/help/img5.jpg" id="help_img">
                        <img src="img/help/img5.jpg">
                    </a>
                </div>
                <div class="help_box_text">
                    <ol>
                        <li><i class="fa fa-lightbulb-o"></i>ほげほげほげほ</li>
                        <li><i class="fa fa-lightbulb-o"></i>ほげほげほげほ</li>
                        <li><i class="fa fa-lightbulb-o"></i>ほげほげほげほ</li>
                        <li><i class="fa fa-lightbulb-o"></i>ほげほげほげほ</li>
                        <li><a href="#top">ページ上部に戻る</a></li>
                    </ol>
                </div>
            </div>
        </div>
        </section>
        <p id="copyright"><small>&copy;2014 イチイチ</small></p>
    </body>
</html>