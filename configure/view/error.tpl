<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="user-scalable=no">
<title>[ちかトイレくん]エラー</title>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<!--▼UA判定用JS-->
<script type="text/javascript" src="./js/get_ua.js"></script>
<script type="text/javascript" src="./js/geo_location.js"></script>
<link rel="shortcut icon" href="img/common/favicon.png">
<link rel="apple-touch-icon" href="img/common/webClipIcon.png">
<link rel="stylesheet" type="text/css" href="http://mplus-fonts.sourceforge.jp/webfonts/mplus_webfonts.css" />
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="./css/common.css">
<link rel="stylesheet" type="text/css" href="./css/error.css">
<!-- 縦 -->
<link rel="stylesheet" media="all and (orientation:portrait)" href="./css/portrait_menu.css">
<!-- 横 -->
<link rel="stylesheet" media="all and (orientation:landscape)" href="./css/landscape_menu.css">
<script type="text/javascript" src="./js/menu.js"></script>
</head>
<body>
{include file='tag_manager.tpl'}
    <header>
    {include file='menu.tpl'}
    </header>
    <section>

    <div class="message_box">
        <div class="message_area">
            <p><i class="fa fa-exclamation-triangle"></i>システムエラーが発生しました。</p>
        </div>
    </div>

    </section>
    <p id="copyright"><small>&copy;2014 イチイチ</small></p>
</body>
</html>
