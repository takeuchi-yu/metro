<div class="pull_menu">
<div class="pull_menu_button open">
</div>
<nav class="pull_menu_nav">
<div class="pull_menu_nav_list another_list">
<p class="menu_t t_sit"><span>menu</span></p>
<ul>
<li><a href="#" onclick="getGeo(location.search.substring(1))">現在地情報再取得</a></li>
<li><a href="{$smarty.const.BASE_URL}">TOPページへ</a></li>
<li><a href="{$smarty.const.BASE_URL}?type={$smarty.const.TYPE_HELP}">ヘルプ</a></li>
</ul>
</div>
<div class="pull_menu_nav_list situation_list">
<p class="menu_t t_sit situation_title"><span>シチュエーション選択</span></p>
<ul>
<li><a href="#" onclick="getGeo('type=situation&s={$smarty.const.SITUATION_MORESOU}')"><img src="img/train_map/theme_danger.png"><span>今すぐ漏れそう</span></a></li>
<li><a href="#" onclick="getGeo('type=situation&s={$smarty.const.SITUATION_INSIDE_GATE}')"><img src="img/train_map/theme_in.png"><span>改札の中にトイレあり</span></a></li>
<li><a href="#" onclick="getGeo('type=situation&s={$smarty.const.SITUATION_OUTSIDE_GATE}')"><img src="img/train_map/theme_out.png"><span>改札の外にトイレあり</span></a></li>
<li><a href="#" onclick="getGeo('type=situation&s={$smarty.const.SITUATION_BABY_CHANGING_TABLE}')"><img src="img/train_map/theme_babyChangingTable.png"><span>ベビーシートあり</span></a></li>
<li><a href="#" onclick="getGeo('type=situation&s={$smarty.const.SITUATION_BABY_CHAIR}')"><img src="img/train_map/theme_babyChair.png"><span>ベビーチェアあり</span></a></li>
<li><a href="#" onclick="getGeo('type=situation&s={$smarty.const.SITUATION_WHEELCHAIR_ASSESIBLE}')"><img src="img/train_map/theme_wheelchair.png"><span>車いす対応トイレ</span></a></li>
<li><a href="#" onclick="getGeo('type=situation&s={$smarty.const.SITUATION_TOILET_FOR_OSTOMATE}')"><img src="img/train_map/theme_daredemo.png"><span>オストメイト対応トイレ</span></a></li>
</ul>
</div>
<p class="pull_menu_nav_close close">閉じる</p>
</nav>
</div>
